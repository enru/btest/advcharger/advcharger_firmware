#include "rcan.h"


#define SET_VOLTAGE_ID      301
#define SET_CURRENT_ID      302
#define TURN_ON_OFF_ID      303
#define STATUS_CHARGER_ID   304

#define CAN_IFACE FDCAN2_BASE
#define CAN_BITRATE  1000000


rcan can;
rcan_frame receive_frame = {0};
rcan_frame send_frame = {.id= STATUS_CHARGER_ID, .type = std_id, .len = 5, .rtr = false, .payload = {0}};

void advcharger_init(void) {
    rcan_start(&can, CAN_IFACE, CAN_BITRATE);
}

void advcharger_rx_loop(void) {

    if(rcan_receive(&can,&receive_frame)){

        if(receive_frame.id == SET_CURRENT_ID){
            //
        }else if(receive_frame.id == SET_VOLTAGE_ID){
            //
        } else if (receive_frame.id == TURN_ON_OFF_ID){
            //
        }
    }
}


void advcharger_tx_loop(void) {

    if (!rcan_is_ok(&can)) {
        rcan_stop(&can);
        HAL_Delay(100);
        rcan_start(&can, CAN_IFACE, CAN_BITRATE);
    }

    if(rcan_is_ok(&can)){
        send_frame.payload[0] |=
        rcan_send(&can, &send_frame);
    }

}