#include "led.h"
#include "main.h"

#define LED_GREEN_PORT LED1_GPIO_Port
#define LED_GREEN_PIN LED1_Pin

#define LED_RED_PORT LED2_GPIO_Port
#define LED_RED_PIN LED2_Pin

void led_red_on(void) {
    HAL_GPIO_WritePin(LED_RED_PORT, LED_RED_PIN, GPIO_PIN_SET);
}

void led_red_off(void) {
    HAL_GPIO_WritePin(LED_RED_PORT, LED_RED_PIN, GPIO_PIN_RESET);
}

void led_red_toggle(void) {
    HAL_GPIO_TogglePin(LED_RED_PORT, LED_RED_PIN);
}

void led_green_on(void) {
    HAL_GPIO_WritePin(LED_GREEN_PORT, LED_GREEN_PIN, GPIO_PIN_SET);
}

void led_green_off(void) {
    HAL_GPIO_WritePin(LED_GREEN_PORT, LED_GREEN_PIN, GPIO_PIN_RESET);
}

void led_green_toggle(void) {
    HAL_GPIO_TogglePin(LED_GREEN_PORT, LED_GREEN_PIN);
}