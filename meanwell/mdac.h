#ifndef __MDAC_H_
#define __MDAC_H_

#include "stdio.h"

#define MAX_DAC_DATA 4095U
#define DAC_CURRENT_CHANEL DAC_CHANNEL_2
#define DAC_VOLTAGE_CHANEL DAC_CHANNEL_1
#define MAX_GAIN_OUTPUT_VOLTAGE 5.05600f
#define VCCA_REF_VOLTAGE 3.336f

void mdac_set_current_chanel(float voltage);
void mdac_set_voltage_chanel(float voltage);


#endif