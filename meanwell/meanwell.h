#ifndef __MEANWELL_H_
#define __MEANWELL_H_

#include "stdbool.h"


#define MEANWELL_MAX_CURRENT 105.0f
#define MEANWELL_MIN_CURRENT 21.0f

#define MEANWELL_MAX_VOLTAGE 48.0f
#define MEANWELL_MIN_VOLTAGE 9.6f



void meanwell_on(void);
void meanwell_off(void);

bool meanwell_ac_is_in_fail(void);
bool meanwell_over_heated(void);
bool meanwell_dc_is_ok(void);
bool meanwell_fan_is_failed(void);

void meanwell_set_current(float current);
void meanwell_set_voltage(float voltage);
void meanwell_set_zero_outs(void);

#endif