#include "stm32l4xx_hal.h"
#include "meanwell.h"
#include "main.h"
#include "mdac.h"


static float convert_voltage_to_output(float voltage);

static float convert_current_to_output(float current);

void meanwell_on(void) {
    HAL_GPIO_WritePin(REM_CTRL_GPIO_Port, REM_CTRL_Pin, GPIO_PIN_SET);
}

void meanwell_off(void) {
    HAL_GPIO_WritePin(REM_CTRL_GPIO_Port, REM_CTRL_Pin, GPIO_PIN_RESET);
}


bool meanwell_ac_is_in_fail(void) {
    return HAL_GPIO_ReadPin(SIG_AC_FAIL_GPIO_Port, SIG_AC_FAIL_Pin) == GPIO_PIN_RESET;
}

bool meanwell_over_heated(void) {
    return HAL_GPIO_ReadPin(SIG_OTP_GPIO_Port, SIG_OTP_Pin) == GPIO_PIN_RESET;
}

bool meanwell_dc_is_ok(void) {
    return HAL_GPIO_ReadPin(SIG_DC_OK2_GPIO_Port, SIG_DC_OK2_Pin) == GPIO_PIN_RESET;
}

bool meanwell_fan_is_failed(void) {
    return HAL_GPIO_ReadPin(SIG_FAN_FAIL_GPIO_Port, SIG_FAN_FAIL_Pin) == GPIO_PIN_RESET;
}

void meanwell_set_current(float current) {
    if (current > MEANWELL_MAX_CURRENT || current < MEANWELL_MIN_CURRENT)
        return;

    float eq_vol = convert_current_to_output(current);
    mdac_set_current_chanel(eq_vol);
}


void meanwell_set_voltage(float voltage) {
    if (voltage > MEANWELL_MAX_VOLTAGE || voltage < MEANWELL_MIN_VOLTAGE)
        return;

    float eq_vol = convert_voltage_to_output(voltage);
    mdac_set_voltage_chanel(eq_vol);

}

void meanwell_set_zero_outs(void) {
    mdac_set_current_chanel(0.0f);
    mdac_set_voltage_chanel(0.0f);
}


static float convert_current_to_output(float current) {
    return MAX_GAIN_OUTPUT_VOLTAGE * current / MEANWELL_MAX_CURRENT;
}

static float convert_voltage_to_output(float voltage) {
    return MAX_GAIN_OUTPUT_VOLTAGE * voltage / MEANWELL_MAX_VOLTAGE;
}


